process.env.NODE_ENV = 'test'

import chai from 'chai'
import chaiHttp from 'chai-http'
import fs, { read } from 'fs'
import path from 'path'
import Reader from '../src/reader'
import Writer from '../src/writer'
import { NatsTransport } from '../src/transport'
import { StringCodec } from 'nats'
import { FastifyInstance } from 'fastify'
import sinon from 'sinon'
import { describe } from 'mocha'
import axios from 'axios'
import { assert } from 'console'
chai.use(chaiHttp)

describe('All test', () => {
    describe('Reader API', () => {
        let reader: Reader
        let app: FastifyInstance | undefined
        let transport = new NatsTransport('localhost')
        let fileName = '/a/b/c/file.png'
        before(async () => {
            sinon.stub(transport, 'connect').callsFake(async () => { })
            sinon.stub(transport, 'send').callsFake(async (...args) => {
                return { status: true, fileName }
            })
            reader = new Reader(transport)
            app = await reader.run(3007)
        })
        after(() => {
            sinon.restore()
        })
        describe('Test /text', async () => {
            it('Valid message', async () => {
                chai.request(app?.server)
                .post('/text')
                .send({ message: 'test message' })
                .end((err, res) => {
                        chai.expect(res).to.have.status(200)
                        chai.expect(res.body).to.be.a('object')
                        chai.expect(res.body).to.have.property('status').eql(true)
                })
            })
            it('Empty message', async () => {
                chai.request(app?.server)
                .post('/text')
                .send({ message: '' })
                .end((err, res) => {
                        chai.expect(res).to.have.status(200)
                        chai.expect(res.body).to.be.a('object')
                        chai.expect(res.body).to.have.property('status').eql(false)
                })
            })
        })
        describe('Test /file', async () => {
            it('Send 1x1 png file', async () => {
                let url = 'http://file.url.com/png'
                let pxPNGBase64= 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII='
                let axiosStub = sinon.stub(axios, 'get').callsFake(async (...args) => {
                    return { data: Buffer.from(pxPNGBase64, 'base64') }
                }).withArgs(url)

            
                chai.request(app?.server)
                .post('/file')
                .send({ url })
                .end((err, res) => {
                        chai.expect(res).to.have.status(200)
                        chai.expect(res.body).to.be.a('object')
                        chai.expect(res.body).to.have.property('status').eql(true, 'status')
                        chai.expect(res.body).to.have.property('fileName').equal(fileName)
                        assert(axiosStub.called)
                })
            })
        }) 
    })

    describe('Test writer', () => {
        let transport = new NatsTransport('localhost')
        afterEach(() => {
            sinon.restore()
        })
        it('Text resolver', async function () {
            let wsWriter = 'tests/testwriterData'
            let fileName = 'testMessage.txt'
            let writer = new Writer(transport, wsWriter, 'queue', fileName)
            let fsStup = sinon.stub(fs.promises, 'appendFile').callsFake(async (...args) : Promise<void> => {
                return
            })
            let sCodec = StringCodec()
            await writer.textResolver(sCodec.encode('test message'))
            let fsWriteFileName = fsStup.lastCall.args[0]
            let fsWriteData = fsStup.lastCall.args[1]
            chai.expect(fsWriteFileName).to.be.a('string')
            chai.expect(fsWriteFileName).to.contain(path.join(wsWriter, fileName))

            chai.expect(fsWriteData).to.be.a('string')
            chai.expect(fsWriteData).to.contain(path.join('test message'))
        })
        it('File resolver', async function () {
            let writerWorkFolder = path.join('tests','testwriterData')
            let pxPNGBase64 = 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII='
            
            let writer = new Writer(transport, writerWorkFolder, 'queue')

            sinon.stub(fs, 'existsSync').callsFake(() => { return true }) // чтобы не создавались  папки
            
            let fsStup = sinon.stub(fs.promises, 'writeFile').callsFake(async (...args) : Promise<void> => {
                return
            })
            let fileName = await writer.fileResolver(Buffer.from(pxPNGBase64, 'base64'), 'file.png')
            chai.expect(fsStup.lastCall.args[0]).to.be.a('string')
            chai.expect(fsStup.lastCall.args[0]).to.contain(writerWorkFolder)
            chai.expect(fsStup.lastCall.args[0]).to.contain('.png')
            chai.expect(fsStup.lastCall.args[0]).to.equal(fileName)
        })
    })
})