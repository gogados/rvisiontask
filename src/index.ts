import Writer from './writer'
import Reader from './reader'
import { NatsTransport } from './transport'
import { FastifyInstance } from 'fastify'
import fs from 'fs'
import dotenv from 'dotenv'
dotenv.config()
const args = process.argv.slice(2)
let transport = new NatsTransport(process.env.SERVER as string,
                                  +(process.env.REQUEST_TIMEOUT as string))
const writerWS = process.env.WRITER_DIR as string

if (!args.length) {
  runReader()
  runWriter()
}
else {
  if (args[0] === 'reader')
    runReader()
  else
    runWriter()
}

async function runReader() {
  let reader = new Reader(transport)
  try {
   await reader.run(+(process.env.API_PORT as string))
  }
  catch (err) {
    process.exit(1)
  }

}
function runWriter() {
    try {
      if (!fs.existsSync(writerWS)){
        fs.mkdirSync(writerWS)
      }
    } catch (err) {
        console.error(err)
        process.exit(1)
    }
  let writer = new Writer(transport, writerWS, process.env.QUEUE as string)
  try {
    writer.run()
  }
  catch (err) {
    process.exit(1)
  }
}