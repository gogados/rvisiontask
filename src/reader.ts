import { StringCodec } from 'nats'
import { Transport } from './transport'
import Fastify, { FastifyInstance } from 'fastify'
import axios from 'axios'
import FileType from 'file-type'

export default class Reader {
    private transport: Transport
    private server: FastifyInstance
    constructor(transport: Transport) {
        this.transport = transport
        this.server = Fastify({ logger: false })
        this.initApi()
    }
    async stop() {
        await this.transport.close()
    }
    async run(port: number): Promise<FastifyInstance | undefined> {
        await this.transport.connect()
        try {
            await this.server.listen(port)
        } catch (err) {
            this.server.log.error(err)
            throw err
        }
        return this.server
    }
    private initApi() {
        const sCodec = StringCodec()      
        this.server.post('/text', async (request) => {
            const data = request.body ? request.body as { message: string } : { message: '' }
            if (!data || !data.message)
                return {
                    status: false,
                    comment: 'The request body must contain message'
                }
            try {
                return await this.transport.send('text', sCodec.encode(data.message))
            }
            catch (err: any){
                return {status: false, comment: err?.toString()}
            }
            
        })
        this.server.post('/file', async (request) => {
            const data = request.body ? request.body as { url: string } : { url: '' }
            let responseData: any
            if (!data || !data.url) {
                responseData = {
                    status: false,
                    comment: 'The request body must contain file url'
                }
            }
            else {
                try {
                    let buffer = await this.loadFile(data.url)
                    let fileType = (await FileType.fromBuffer(buffer))?.ext
                    responseData = await this.transport.send(`file.${fileType}`, buffer)
                }
                catch (err: any) {
                    responseData = {
                        status: false,
                        comment: err?.toString()
                    }
                }
            }
            return responseData
        })
    }
    
    private async loadFile(url: string): Promise<Buffer> {
        let res = await axios.get(url, { responseType: 'arraybuffer' })
        return Buffer.from(res.data, 'binary')
    }
}