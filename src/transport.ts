const { connect, JSONCodec } = require('nats')
const jsonCodec = JSONCodec()
import { performance } from 'perf_hooks'
import randomstring from 'randomstring'

export interface Transport {
    connect(): Promise<void>
    close(): Promise<void>
    send(subject: string, data: any): Promise<any>
    on(subject: string, cb: (data: any, subject: string ) => Promise<any>, options?: any): void
}

export class NatsTransport implements Transport {
    readonly server: string
    client?: any
    options: {
        timeout: number
        reconnect: boolean
        maxReconnectAttempts: number
    }
    constructor(server: string, timeout?: number) {
        this.server = server
        this.options = {
            timeout: timeout ? timeout : 5000,
            reconnect: true,
            maxReconnectAttempts: 10
        }

    }
    async send(subject: string, data: any) {
        let size = Buffer.byteLength(data)    
        if (size > this.client.info['max_payload'])
            throw new Error('Payload too large')
        if(!this.client)
            throw new Error('Nats client undefined')
        try {
            if (this.client?.isClosed())
                await this.connect()
            let begin = performance.now()
            let res = await this.client.request(subject, data, this.options)
            let resData: any = jsonCodec.decode(res.data)            
            return { ...resData, time: Math.round( performance.now() - begin ) }
        }
        catch (e) {
            console.log(e)
            return { status: false }
        }
    }
    on(subject: string, cb: (data: any, subject: string) => Promise<any>, options?: any) {
        const subscribe = this.client.subscribe(subject, options);
        (async () => {        
            for await (const req of subscribe) {
                try {
                    let replyData = await cb(req.data, req.subject)
                    this.client.publish(req.reply, replyData)
                } catch(e) {
                    this.client.publish(req.reply, e)
                }
            }
        })()
    }
    async connect() {
        if ( this.client && !this.client?.isClosed() )
            throw new Error('Nats client already exist')

        this.client = await connect({ servers: this.server, noRandomize: true })
        console.log('NATS transport is running...')
    }
    async close() {
        console.log('NATS transport is closing...')
        return await this.client.drain()    
    }
}