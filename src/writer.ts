import { StringCodec, JSONCodec } from 'nats'
import { v4 as uuidv4 } from 'uuid'
import { Transport } from './transport'
import fs from 'fs'
import path from 'path'
const sCodec = StringCodec()
const jsonCodec = JSONCodec()
const sha256 = require('crypto-js/sha256')

export default class Writer {
    private transport: Transport
    private id: any
    private workSpace: any
    private queue: string
    private messageFile: string
    constructor(transport: Transport, workSpace: string, queue: string, messageFile?: string) {
        this.transport = transport
        this.id = uuidv4()
        this.workSpace = workSpace
        this.queue = queue
        this.messageFile = messageFile ? messageFile : 'icommingMessage.txt'
    }
    async stop() {
        return this.transport.close()
    }
    async run() {
        await this.transport.connect()
        console.log('Writer is running...')
        const options = { queue: this.queue }
        this.transport.on('text', async (data: any, subject: string) => { 
            return new Promise((resolve) => {
                setTimeout(async () => {
                    try {                  
                        console.log(`Writer [${this.id}] received message ${sCodec.decode(data)}`)
                        await this.textResolver(data)
                        resolve(jsonCodec.encode({status: true}))
                    }
                    catch (e) {
                        console.error(`Failed to save message : ${e}`)
                        resolve(jsonCodec.encode({status: false}))
                    }
            }, +(process.env.WRITER_FAKE_DELAY as string))
            })

        }, options)
        this.transport.on('file.*', async (data: any, subject: string) => {
            return new Promise((resolve) => {
                setTimeout(async () => {
                    try {
                        console.log(`Writer [${this.id}] received file`)
                        let fileName = await this.fileResolver(data, subject)
                        resolve(jsonCodec.encode({ status: true, fileName }))
                    }
                    catch (e) {
                        console.error(`Failed to save file : ${e}`)
                        resolve(jsonCodec.encode({ status: false }))
                    }
                }, +(process.env.WRITER_FAKE_DELAY as string))
            })
        }, options)
    }
    async textResolver(data: any) {
        let line = `${(new Date()).toUTCString()} : ${sCodec.decode(data)}\n`
        await fs.promises.appendFile(path.join(this.workSpace, this.messageFile), line)
    }
    async fileResolver(data: any, subject: string) {
        let ext = subject.split('.')[1]
        let fileName = this.generateFilePath(data)
        fileName = `${fileName}.${ext}`
        await fs.promises.writeFile(fileName, data)
        return fileName
    }
    private generateFilePath(file: Buffer): string {
		const hash = String(sha256(file.toString('base64')))
		let splitHash = hash.match(/.{1,2}/g)?.splice(0, 4)
		const fileName = hash.substr(0,5)
		let location = this.workSpace
		splitHash?.map(el => {
			location = path.join(location, el)
			if (!fs.existsSync(location)) fs.mkdirSync(location, '0777')
			return el
		})
        return path.join(location,fileName)
    
    }
}
